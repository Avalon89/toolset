/*******************************************************************************
 * \copyright   This file is part of a toolset for anisotropic calculations
 * \copyright   by Andreas Schmidt
 * 		for questions (aschmidt@mpa-garching.mpg.de)
 *******************************************************************************/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <vector>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sf_gamma.h>
/*! \file Elliptical.cc
 *  \brief Gravitational calculation for elliptical potential and force
 *
 *  This file contains the calculation of the non-spherical gravity potential
 *  Also includes calculations for the force
 */

using namespace std;
#define TEST 0

#define NGRID 16
#define NTAB 10000
#define RCUT 0.5
#define RMAX 2.0

double Integrand_order(double r, int m, double rs, double alpha0);
vector<double> do_Im_interpolation(double r);
vector<double> do_dIm_interpolation(double r);
/*
 * This is the function from the integral
 */
double Integrand_order(double r, int m, double rs, double alpha0)
{
    double x = r*r/(4*rs*rs*alpha0*alpha0);
    double pre = pow(2.*rs , m-2); 
    double test = pre * (gsl_sf_gamma_inc((m-2.)/2, 0) - gsl_sf_gamma_inc((m-2.)/2.,x));
    return test;
}

double derivative_Im(double r, int m, double rs, double alpha0)
{
    double x = r*r/(4*rs*rs*alpha0*alpha0);
    double pre = r/4. *pow(rs, m);
    double fac1 = 8*alpha0*alpha0 * exp(-1.*x) * pow(1./(rs*rs*alpha0*alpha0), m/2.) * pow(r,m-2.);
    double fac2 = pow(2.,m)*(m-2) * (gsl_sf_gamma_inc((m-2.)/2, 0) - gsl_sf_gamma_inc((m-2.)/2.,x)) /(rs*rs);
    double dIm = pre * (fac1 - fac2);
    return dIm;
}
      
/*
 *   Force = x_{i}/r Sum(fac_m I'_m(r)) + Sum(fac'_m I_m(r))
 *                   I                           II
 *  Need to change the first part to a non loop implementation
 */

vector<double> elliptical_force_long(double x[3], double alpha[3], double rs, int order)
{
    double alpha0 = pow(alpha[0]*alpha[1]*alpha[2], 1./3.);
    double I3,I5, I7, I9, I11, Norm, da[3], sum_da;
    vector<double> Force (3);
    double r = sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
    int l,m;
    for (int i = 0; i < 3; i++)
    {
        da[i] = alpha[i]-alpha0;
    }
    sum_da = da[0]+da[1]+da[2];
    Norm = pow(M_PI, -0.5)/(2*rs);
    vector<double> Im (5), dIm (5);
    dIm = do_dIm_interpolation(r);
    Im = do_Im_interpolation(r);
    I3 = Im[0];
    I5 = Im[1];
    I7 = Im[2];
    I9 = Im[3];
    I11 = Im[4];
    // Im
    for(int k = 0; k < 3; k++)// Three axis
    {
        Force[k] = dIm[0];
        Force[k] += -dIm[1] * alpha0 * sum_da;
        Force[k] += dIm[2] / (2.*rs*rs) * alpha0 * (da[0]*x[0]*x[0] + da[1]*x[1]*x[1] + da[2]*x[2]*x[2]);
        if(order >= 2)
        {
            Force[k] += dIm[1] * (-0.5) * (da[0]*da[0] + da[1]*da[1] + da[2]*da[2]); //I5 second order term
            Force[k] += dIm[2] * (alpha0 * alpha0 * (1.5 * (da[0]*da[0] + da[1]*da[1] + da[2]*da[2]) + (da[0]*da[1] + da[1]*da[2] + da[0]*da[2])) 
                        + (1./(4*rs*rs)*(x[0]*x[0]*da[0]*da[0] + x[1]*x[1]*da[1]*da[1] + x[2]*x[2]*da[2]*da[2])));

            Force[k] += dIm[3] * -1.*alpha0*alpha0/(2*rs*rs) * (3*(x[0]*x[0]*da[0]*da[0] + x[1]*x[1]*da[1]*da[1] + x[2]*x[2]*da[2]*da[2]) 
                        + (da[0]*da[1] * (x[0]*x[0] + x[1]*x[1]) + da[0]*da[2] * (x[0]*x[0] + x[2]*x[2]) + da[1]*da[2] * (x[1]*x[1] + x[2]*x[2])));
                        
            Force[k] += dIm[4] * alpha0*alpha0/(8*rs*rs*rs*rs) * (x[0]*x[0]*x[0]*x[0]*da[0]*da[0] + x[1]*x[1]*x[1]*x[1]*da[1]*da[1] + x[2]*x[2]*x[2]*x[2]*da[2]*da[2] + 
                        2*(x[0]*x[0]*da[0]*(x[1]*x[1]*da[1] + x[2]*x[2]*da[2]) + x[1]*x[1]*x[2]*x[2]*da[1]*da[2]));
        }
        Force[k] *= x[k]/r;
        // Now the derivative of the factors: II
        Force[k] += I7/(rs*rs) * alpha0 * da[k]*x[k];
    }
    if(order >= 2)
    {
        // df_{m}/dx_{1}
        Force[0] += I7 * 0.5 * da[0]*da[0] * x[0]/(rs*rs);
        Force[0] += I9 * (-1) * alpha0 * alpha0 * x[0] * da[0]* (da[0] * 3. + da[1] + da[2])/(rs*rs);
        Force[0] += I11 * 0.5 * alpha0 * alpha0 * x[0] * da[0] * (da[0] * x[0]*x[0] + da[1] * x[1]*x[1] + da[2] * x[2]*x[2])/(pow(rs,4.));

        Force[1] += I7 * 0.5 * da[1]*da[1] * x[1]/(rs*rs);
        Force[1] += I9 * (-1) * alpha0 * alpha0 * x[1] * da[1]* (da[1] * 3. + da[0] + da[2])/(rs*rs);
        Force[1] += I11 * 0.5 * alpha0 * alpha0 * x[1] * da[1]* (da[1] * x[1]*x[1] + da[0] * x[0]*x[0] + da[2] * x[2]*x[2])/(pow(rs,4.));
        
        Force[2] += I7 * 0.5 * da[2]*da[2] * x[2]/(rs*rs);
        Force[2] += I9 * (-1) * alpha0 * alpha0 * x[2] * da[2]* (da[2] * 3. + da[0] + da[1])/(rs*rs);
        Force[2] += I11 * 0.5 * alpha0 * alpha0 * x[2] * da[2]* (da[2] * x[2]*x[2] + da[0] * x[0]*x[0] + da[1] * x[1]*x[1])/(pow(rs,4.));
    }
    for(int k = 0; k < 3; k++)
        Force[k] *= Norm;
    
    return Force;
}

vector<double> elliptical_force_short(double x[3], double alpha[3], double rs, int order)
{
    vector<double> Force_long = elliptical_force_long(x, alpha, rs, order);
    vector<double> Force_short (3);
    double r2 = x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
    double r = sqrt(r2);
    for (int i = 0; i < 3; i++)
    {
        Force_short[i] = x[i]/(r*r2) * (1. - Force_long[i] * (r*r2)/x[i]);
    }
    return Force_short;
}

/**
 * This is the main function that calculates the long range potential
 * The arguments are:
 *                      Pos x[3]    : of the particle
 *                      Distance r  : from the particle that does the potential calculation
 *                      alpha [3]   : Stretch factors for anisotropic scale factors
 *                      rs          : Cut off radius /ASMTH
 *                      order       : Order of approximation
 */

double elliptical_long(double x[3], double alpha[3], double rs, int order)
{
    double alpha0 = pow(alpha[0]*alpha[1]*alpha[2], 1./3.);
    double I5, I7, I9, I11, Phi, Norm, da[3], sum_da;
    double r = sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);

    for (int i = 0; i < 3; i++)
    {
        da[i] = alpha[i]-alpha0;
    }
    sum_da = da[0]+da[1]+da[2];
    Norm = pow(M_PI, -0.5)/(2*rs);
    vector<double> Im (5);
    Im = do_Im_interpolation(r);
    if(TEST == 1)
    {
        ofstream file_test;
        file_test.open("Im_interpolation_test.txt",ios::out | ios::app);
        file_test << x[0] << "\t" << x[1] << "\t" << x[2] << "\t" << Im[0] << "\t" << Im[1] << "\t" << Im[2] << "\t" << Im[3] << "\t" << Im[4] << "\t";
        file_test << Integrand_order(r, 3, rs, alpha0) << "\t" << Integrand_order(r, 5, rs, alpha0) << "\t" << Integrand_order(r, 7, rs, alpha0) << "\t"
            << Integrand_order(r, 9, rs, alpha0) << "\t" << Integrand_order(r, 11, rs, alpha0) << "\n";
        file_test.close();
    }
    Phi = Im[0];
    if(order >= 1)
    {
        I5 = Im[1];
        I7 = Im[2];
        Phi += -I5 * alpha0 * sum_da;
        Phi += I7 * 0.5/ (rs*rs) * alpha0 * (da[0]*x[0]*x[0] + da[1]*x[1]*x[1] + da[2]*x[2]*x[2]);
    }
    if(order >= 2)
    {
        I9 = Im[3];
        I11 = Im[4];

        Phi += I5 * (-0.5) * (da[0]*da[0] + da[1]*da[1] + da[2]*da[2]); //I5 second order term
        Phi += I7 * (alpha0 * alpha0 * (1.5 * (da[0]*da[0] + da[1]*da[1] + da[2]*da[2]) + (da[0]*da[1] + da[1]*da[2] + da[0]*da[2])) 
                    + (1./(4*rs*rs)*(x[0]*x[0]*da[0]*da[0] + x[1]*x[1]*da[1]*da[1] + x[2]*x[2]*da[2]*da[2])));

        Phi += I9 * -1.*alpha0*alpha0/(2*rs*rs) * (3*(x[0]*x[0]*da[0]*da[0] + x[1]*x[1]*da[1]*da[1] + x[2]*x[2]*da[2]*da[2]) 
                    + (da[0]*da[1] * (x[0]*x[0] + x[1]*x[1]) + da[0]*da[2] * (x[0]*x[0] + x[2]*x[2]) + da[1]*da[2] * (x[1]*x[1] + x[2]*x[2])));
                    
        Phi += I11 * alpha0*alpha0/(8*rs*rs*rs*rs) * (x[0]*x[0]*x[0]*x[0]*da[0]*da[0] + x[1]*x[1]*x[1]*x[1]*da[1]*da[1] + x[2]*x[2]*x[2]*x[2]*da[2]*da[2] + 
                    2*(x[0]*x[0]*da[0]*(x[1]*x[1]*da[1] + x[2]*x[2]*da[2]) + x[1]*x[1]*x[2]*x[2]*da[1]*da[2]));
    }
    return Norm * Phi;
}

double elliptical_short(double x[3], double alpha[3], double rs, int order)
{
    double lx[3];
    lx[0] = x[0] * alpha[0];
    lx[1] = x[1] * alpha[1];
    lx[2] = x[2] * alpha[2];
    double phi_long = elliptical_long(lx, alpha, rs, order);
    double xx = 0;
    for(int i = 0; i < 3; i++)
    {
	    xx += lx[i]*lx[i];
    }
    double phi_comp = 1./sqrt(xx+1.e-10);
    return (phi_comp - phi_long);
}

static vector<double> Im_intern(5);
static vector<vector <double>> Im_table;
static vector<vector <double>> dIm_table;
void initialize_table(double alpha0, double rs)
{
    for (int i = 0; i <= NTAB; i++)
    {
        double u = (RMAX / NTAB) * i;
        Im_intern[0] = Integrand_order(u,3,rs, alpha0);
        Im_intern[1] = Integrand_order(u,5,rs, alpha0);
        Im_intern[2] = Integrand_order(u,7,rs, alpha0);
        Im_intern[3] = Integrand_order(u,9,rs, alpha0);
        Im_intern[4] = Integrand_order(u,11,rs, alpha0);
        Im_table.push_back(Im_intern);
        Im_intern[0] = derivative_Im(u,3,rs, alpha0);
        Im_intern[1] = derivative_Im(u,5,rs, alpha0);
        Im_intern[2] = derivative_Im(u,7,rs, alpha0);
        Im_intern[3] = derivative_Im(u,9,rs, alpha0);
        Im_intern[4] = derivative_Im(u,11,rs, alpha0);
        dIm_table.push_back(Im_intern);
    }
}

vector<double> do_Im_interpolation(double r)
{
    double tabentry = r * NTAB/RMAX;
    int tabindex = (int)tabentry;
    vector<double>fac_pot(5);
    if(tabindex < NTAB)
    {
        double tabweight = tabentry - tabindex;
        for(int i = 0; i < 5; i++)
        {
            int m = 2*i+1;
            fac_pot[i] = (1.0 - tabweight) * Im_table[tabindex][i] + tabweight * Im_table[tabindex + 1][i];
            fac_pot[i] /= pow(r,m);
        }
    } 
    else
    {
        cout << "tabindex out of range... tabindex = " << tabindex << " NTAB = " << NTAB << endl;
        exit(-1);
    }
    return(fac_pot);
}

vector<double> do_dIm_interpolation(double r)
{
    double tabentry = r * NTAB/RMAX;
    int tabindex = (int)tabentry;
    vector<double>fac_force(5);
    if(tabindex < NTAB)
    {
        double tabweight = tabentry - tabindex;
        int m;
        for(int i = 0; i < 5; i++)
        {
            m = 2*i+3;
            fac_force[i] = (1.0 - tabweight) * dIm_table[tabindex][i] + tabweight * dIm_table[tabindex + 1][i];
            fac_force[i] /= pow(r,m);
        }
    } 
    else
    {
        cout << "tabindex out of range... tabindex = " << tabindex << " NTAB = " << NTAB << endl;
        exit(-1);
    }
    return(fac_force);
}

/*
 * Setup grid between -1 and 1
 */
int main(int argv, char **argc)
{  
    int NGrid = NGRID;
    double alpha[3] = { 1.0, 1.0, 1.0 };
    int order = 2;
    cout << argv << endl;
    double rs = RCUT;
    if(argv >= 2)
    {
	cout << argc[0] << "\t" << argc[1] << endl;
	NGrid = atoi(argc[1]);
    }
    if(argv >=3)
    {
        order = atoi(argc[2]);
    }
    if(argv >= 6)
    {
        alpha[0] = atof(argc[3]);
        alpha[1] = atof(argc[4]);
        alpha[2] = atof(argc[5]);
    }
    if(argv >= 7)
	rs = atof(argc[6]);

    double test_r = sqrt(3.);
    cout << "alpha = (" << alpha[0] << " , " << alpha[1] << " , " << alpha[2] << ") rs = "<< rs <<"\n";
    double alpha0 = pow(alpha[0]*alpha[1]*alpha[2], 1./3.);
    initialize_table(alpha0, rs);
    cout << "Size of table in memory: " 
    << (sizeof(double) * Im_table.size()*Im_table[0].size() + sizeof(vector<double>) + sizeof(vector<vector<double>>))/1024. << endl;

    double lower = -1., upper = 1.;
    double spaceing = (upper - lower)/(NGrid-1.);
    double x[NGrid][NGrid][NGrid][3];
    double phi[NGrid][NGrid][NGrid];
    double phi_long[NGrid][NGrid][NGrid];
    double force_long[NGrid][NGrid][NGrid][3];
    double force_short[NGrid][NGrid][NGrid][3];
    vector<double> tmp_force (3);

    double lx[3];
    ofstream file_pot, file_pot_long, file_force_long, file_force_short;
    file_pot.open ("potential_c_short.txt");
    file_pot_long.open("potential_c_long.txt");
    file_force_long.open("Force_c_long.txt");
    file_force_short.open("Force_c_short.txt");
    cout << "Using order " << order << endl;
    for (int i = 0; i<NGrid; i++)
        for(int j = 0; j < NGrid; j++)
            for(int k = 0; k < NGrid; k++)
            {
                x[i][j][k][0] = (i * spaceing) -1;
                x[i][j][k][1] = (j * spaceing) -1;
                x[i][j][k][2] = (k * spaceing) -1;
                phi[i][j][k] = elliptical_short(x[i][j][k], alpha, rs, order);
                lx[0] = x[i][j][k][0] * alpha[0];
                lx[1] = x[i][j][k][1] * alpha[1];
                lx[2] = x[i][j][k][2] * alpha[2];
                tmp_force = elliptical_force_short(lx, alpha, rs, order);
                for(int l = 0; l < 3; l++)
                    force_short[i][j][k][l] = tmp_force[l];
                phi_long[i][j][k] = elliptical_long(lx, alpha, rs, order);
                tmp_force = elliptical_force_long(lx, alpha, rs, order);
                for(int l = 0; l < 3; l++)
                    force_long[i][j][k][l] = tmp_force[l];
                if(k == NGrid/2)
                    {
                        file_pot << phi[i][j][k] << "\t";
                        file_pot_long << phi_long[i][j][k] << "\t";
                        file_force_long << force_long[i][j][k][0] << "\t" << force_long[i][j][k][1] << "\t" << force_long[i][j][k][2] << "\t";
                        file_force_short << force_short[i][j][k][0] << "\t" << force_short[i][j][k][1] << "\t"<< force_short[i][j][k][2] << "\t";
                        if(j == NGrid-1)
                        {
                            file_pot << "\n";
                            file_pot_long << "\n";
                            file_force_long << "\n";
                            file_force_short << "\n";
                        }
                    }
                }
    file_pot.close();
    file_pot_long.close();
    file_force_long.close();
    file_force_short.close();

    return 0;
}
